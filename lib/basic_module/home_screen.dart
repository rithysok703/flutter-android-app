import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  // const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    String pic = "https://www.gqmiddleeast.com/cloud/2023/11/09/6f690089947a98659643e2ce8f5f0200-647x1024.png";
    String img = "https://cdn.moviefone.com/admin-uploads/posters/kingdomoftheplanetofheapes-movie-poster_1698963345.jpg?d=360x540&q=60";
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        // leading: Icon(Icons.home),
        title: Text("Home Screen"),
        backgroundColor: Colors.pink,
      ),
      drawer: Drawer(
        backgroundColor: Color.fromARGB(255, 9, 81, 133),
        child: ListView(
          children: [
            DrawerHeader(child: Image.network("https://www.acledasecurities.com.kh/as/assets/layout/logo1.png")),
            ListTile(
              leading: Icon(Icons.home),
              title: Text("Home"),
              trailing: Icon(Icons.navigate_next),
            ),
            ListTile(
              leading: Icon(Icons.call),
              title: Text("Contact"),
              trailing: Icon(Icons.navigate_next),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Account"),
              trailing: Icon(Icons.navigate_next),
            ),
          ],
        ),
      ),
      endDrawer: Drawer(
        child: Image.network(img, fit: BoxFit.cover,),
      ),
      body: Container(
        child: Image.network(pic, fit: BoxFit.cover,),
      ),
    );
  }
}
