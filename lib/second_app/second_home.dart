import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class SecondApp extends StatelessWidget {
  const SecondApp({super.key});

  @override
  Widget build(BuildContext context) {
    String pic = "https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper-thumbnail.png";
    String bg ="https://img.freepik.com/free-vector/gradient-liquid-mobille-wallpaper_79603-461.jpg";
    return Scaffold(
      appBar: AppBar(

        title: const Text("Good Morning"),
        titleTextStyle: const TextStyle(
          fontFamily: "Arial",
          color: Colors.black,
          fontSize: 24,
        ),

        backgroundColor: Colors.white,

        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.home,
              size: 25.0,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.pink,
          image: DecorationImage(
            image: NetworkImage(bg),
            fit: BoxFit.cover,
          ),
        ),
        // width: 200,
        // height: 200.0,
        child: Container(
          width: 200.0,
          height: 200.0,
          decoration:  BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage(pic),
              fit: BoxFit.cover,
            ),
            gradient: const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.grey, Colors.yellow, Colors.orange,],
            ),
            color: Colors.deepPurple,
            border: Border.all(width: 10, color: Colors.black),
            boxShadow: [BoxShadow(
              offset: const Offset(15.0, 15.0),
              color: Colors.red.withOpacity(0.5),
              blurRadius: 10.0,
            ),],
          ),

        ),

      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blueAccent,
        shape: const CircleBorder(),
        splashColor: Colors.deepPurpleAccent,
        onPressed: () {},
        child: const Icon(Icons.qr_code_scanner_outlined),

      ),

      bottomNavigationBar: BottomAppBar(
        notchMargin: 10,
        shape: const CircularNotchedRectangle(),

        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(onPressed: () {}, icon: const Icon(Icons.payment, color: Colors.indigo, size: 30),),
            IconButton(onPressed: () {}, icon: const Icon(Icons.history, color: Colors.indigo, size: 30),),
            const SizedBox(width: 40),
            IconButton(onPressed: () {}, icon: const Icon(Icons.add_shopping_cart, color: Colors.indigo, size: 30),),
            IconButton(onPressed: () {}, icon: const Icon(Icons.widgets, color: Colors.indigo, size: 30),),
          ],
        ),

      ),

    );
  }

}

