import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        leading: IconButton(icon: Icon(Icons.home), onPressed: () { }, color: Colors.white,),
        title: Text("MR K Movies"),
        titleTextStyle: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white, fontFamily: 'Arial'),

      ),

      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(onPressed: () {}, icon: Icon(Icons.home), color: Colors.white,),
            IconButton(onPressed:() {}, icon: Icon(Icons.search), color: Colors.white,),
            IconButton(onPressed:() {}, icon: Icon(Icons.person), color: Colors.white,),
            IconButton(onPressed:() {}, icon: Icon(Icons.settings), color: Colors.white,),
          ],

        ),

      ),


    );
  }
}
