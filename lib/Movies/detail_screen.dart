// TODO Implement this library.

import 'package:flutter/material.dart';


class DetailScreen  extends StatefulWidget{
  String item;
  DetailScreen(this.item);

  @override
  State<StatefulWidget> createState() => _DetailScreenState();

}
class _DetailScreenState extends State<DetailScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        title: Text("Detial Screen"),

      ),
      body: Container(
        alignment: Alignment.topCenter,
        child: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Image.network(
            this.widget.item,
            fit: BoxFit.cover,
          ),
        ),
      ),


    );

  }


}
