

import 'package:flutter/material.dart';
import '../Movies/movie_data.dart';
import 'details_page.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movies'),
      ),
      body: 
          ListView.builder(
                  itemCount: movieList.length,
                  itemBuilder: (context, index) {
                    final movie = movieList[index];
                    return ListTile(
          leading: Image.network(
            movie.image,
            width: 50,
            height: 50,
            fit: BoxFit.cover,
          ),
          title: Text(movie.title),
          subtitle: Text('Rating: ${movie.rate}'),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MovieDetailScreen(movie: movie),
              ),
            );
          },
                    );
                  },
                ),
      
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
        ],
      ),
    );
  }
}
