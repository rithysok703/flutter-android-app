
import 'dart:io';

void main(){
  List<String> products = [
    "iPhone 15 Pro Max 128GB",
    "Samsung Galaxy Ultra S24",
    "Apple iPhone 13 64GB Black",
    "iPhone 14 Pro Max 128GB",
    "Galaxy S22 64GB",
    "iPhone 14 128GB Blue",
    "iPhone 15 Pro Max 128GB Black Titanium",
    "Samsung Galaxy S23",
    "Oppo Pro G12",
  ];

  String search1 = "iphone 128GB";
  String search2 = "iphone pro max";
  String search3 = "iphone black";
  String search4 = "samsung S24";

  List<String> fsearch = products.where((product) {
    List<String> searchsplit = search1.split(' ');
    return searchsplit.every((word) =>product.toLowerCase().contains('iphone')) &&
        searchsplit.every((word) => product.toLowerCase().contains('128'));
  }).toList();

  print("Seach1 : $fsearch");

  print('Enter a product name to search for:');
  String userInput = stdin.readLineSync()!;
  List<String> searchWords = userInput.split(' ');
  List<String> result = products.where((product) {
    return searchWords.every((word) => product.toLowerCase().contains(word.toLowerCase()));
  }).toList();


  print('\nSearch result:');
  if (result.isNotEmpty) {
    for (String item in result) {
      print(item);

    }
  } else {
    print('No matching products found');
  }
}