
import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  int a = 10.5.toInt();
  print(a);

  String t1 = "10.5"; // convert to double
  double n = double.parse(t1);
  String t2 = "10"; //convert to int
  int n1 = int.parse(t2);
  String t3 = "false"; //convert to bool
  bool n2 = bool.parse(t3);

  print(n + n1);
  print("t1 $n\n""t2 $n1");
  print(n2);


  //
  double temp = 41.5;
  String temps = temp.toString();


  Scaffold(appBar: "AppBar");
  Scaffold(drawer: "Drawer");
  Scaffold(body: "Body", appBar: "AppBar");

}

void Scaffold({
  String appBar = "appBar",
  String body = "body",
  String drawer = "drawer",
}) {}
